import React,{ Component} from 'react'
import {Avatar} from 'react-native-elements'
import axios from 'axios'
import {Header, Icon} from 'react-native-elements'
import {Button} from 'react-native-elements';
import { 
  Text,
  View,
  Image,
  Platform,
  ScrollView,
  Animated,
  StyleSheet,
  TouchableOpacity,
  Linking,
} from 'react-native'
import { Cell, Section, TableView } from "react-native-tableview-simple";
/*
Creates a Repo Screen, currently set to pull 'shmarka' repo data
*/
export default class RepositoriesScreen extends React.Component {
  static navigationOptions = {
    title: 'Repositories',
    headerStyle: {
          backgroundColor: '#282828',
      },  
      headerTintColor: '#fff',

  };

  /* 
   Constructor for the props selected for this page' state
   */
  constructor(props){
    super(props);
    this.state = {
      repoJSON : '',
      arrayOfCells: [],
      arrayOfURLS  : []
    }
    this.getRepos = this.getRepos.bind(this)
    this.createDynamicCells = this.createDynamicCells.bind(this)
  }
  /*
  This function takes a username and uses github api to query for necessary information about their repos
  param user_name : username of the user to be called in get request
  */
  getRepos(user_name){
    axios.get('https://api.github.com/users/' + user_name + '/repos')
    .then((res)=>{
      this.setState({repoJSON : res.data})

       this.createDynamicCells()
    })
  }

  /*
  Creates and populates Cells for simple-table-view of the queried repos
  */
  createDynamicCells(){
    var tempCellArray = []
    //var repo_urls = []
    for(var i = 0; i < this.state.repoJSON.length; i++){
      //repo_urls.push()
      let repo_url = this.state.repoJSON[i]["html_url"]
      tempCellArray.push(
        <TouchableOpacity>
          <Cell
            cellStyle="Subtitle"
            title={this.state.repoJSON[i]["full_name"]}
            titleTextColor="#2193FF"
            detail = {'Owner: ' + this.state.repoJSON[i]["owner"]["login"] + '   Description: ' + this.state.repoJSON[i]["description"]}
            onPress={() => Linking.openURL(repo_url)}
            
          ></Cell>
        </TouchableOpacity>
      )
      
    }
    this.setState({arrayOfCells : tempCellArray})
  }

  /*
  Wrapper for getRepos, makes sure webpage is created first before populating
  */
  componentDidMount(){
    this.getRepos('shmarka');
  }


  render() {
    return (
      <ScrollView style={styles.container}>
        /*
        Table-view of clickable repos
        */
        <Section header= "PUBLIC" >
          <TableView>
            {this.state.arrayOfCells}
          </TableView>
        </Section>
      </ScrollView>
    );
  }
}

/*
Various styles to select from
*/
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: 'light-blue',
  },
});
