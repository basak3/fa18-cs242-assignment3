import React,{ Component} from 'react'
import {Avatar} from 'react-native-elements'
import axios from 'axios'
import {Header, Icon} from 'react-native-elements'
import {Button} from 'react-native-elements';
import { 
	Text,
	View,
	Image,
	Platform,
	ScrollView,
	Animated,
	StyleSheet,
	TouchableOpacity,
} from 'react-native'
const profileImage = require('/Users/ArkaBasak/cs242_3/images/user-profile.jpg'); 

/*
Creates a Profile Screen, currently set to pull 'shmarka' data
*/
export default class ProfileScreen extends Component {

	static navigationOptions = {
		//header:null,
		title:'Profile',
		headerStyle: {
      		backgroundColor: '#282828',
    	},	
    	headerTintColor: '#fff',
    	
	 };

	 /* 
	 Constructor for the props selected for this page' state
	 */
	constructor(props){
		super(props);
		this.state = {
			username: '',
			name: '',
			repoCount: 0,
			followers: 0 , 
			following: 0,
			email:'',
			avatar_link: 'hi',
			website : '',
			bio: '',
			lastupdated:'',
			created:'',
		}
		this.getUser = this.getUser.bind(this)
	}
	/*
	This function takes a username and uses github api to query for necessary information
	param user_name : username of the user to be called in get request
	*/
	getUser(user_name){
		axios.get('https://api.github.com/users/' + user_name)
		.then((res)=>{
			this.setState({username : res.data['login']})
			this.setState({name : res.data['name']})
			this.setState({repoCount : res.data['public_repos']})
			this.setState({followers : res.data['followers']})
			this.setState({following : res.data['following']})
			this.setState({email : res.data['email']})
			this.setState({avatar_link : res.data['avatar_url']})
			this.setState({website: res.data['blog']})
			this.setState({bio: res.data['bio']})
			this.setState({created: res.data['created_at']})
			this.setState({lastupdated: res.data['updated_at']})
			})

	}

	/*
	Wrapper for getUser, makes sure webpage is created first before populating
	*/
	componentDidMount(){
		this.getUser('shmarka');
	}

  render() {

    let pic = {

    	uri: this.state.avatar_link

    };
    return (
	    <ScrollView>
			<View style = {styles.container}>
				/*
				Profile box
				*/
				<View style={{flexDirection:'row'}}>
					/*
					Profile image
					*/
					<View style = {styles.profileBox}>
		    			<Image
							style={styles.profImage} 
		      				source={{uri: this.state.avatar_link}}
		      			/>
		    		</View>
		    		<View style={{flexDirection:'column'}}>
		    			/*
						Row of Touchable Buttons for repos, followers, following
						*/
			    		<View style={{flexDirection:'row'}}>
							<TouchableOpacity style ={styles.profilePanelStyle}
								onPress={() => this.props.navigation.navigate('Repos')}>
								<Text style = {styles.buttonTextStyle}>{this.state.repoCount}</Text>
								<Text style = {styles.labelStyle}>Repos</Text>
							</TouchableOpacity>
							<TouchableOpacity style ={styles.profilePanelStyle}
								onPress={() => this.props.navigation.navigate('Followers')}>
								<Text style = {styles.buttonTextStyle}>{this.state.followers}</Text>
								<Text style = {styles.labelStyle}>Followers</Text>
							</TouchableOpacity>
							<TouchableOpacity style ={styles.profilePanelStyle}
								onPress={() => this.props.navigation.navigate('Following')}>
								<Text style = {styles.buttonTextStyle}>{this.state.following}</Text>
								<Text style = {styles.labelStyle}>Following</Text>
							</TouchableOpacity>
						</View>
						
						
		    		</View>
		    		
		    	</View>
		    	/*
				User Bio
				*/
		    	<View style={{flexDirection:'row'}}>
					<View>
						<Text style = {{fontSize:15,  color: '#476380', textAlign: 'center'}}>{this.state.bio}</Text>
					</View>
				</View>
				<View style={{flexDirection:'row'}}>
					<View>
						<Text style = {{fontSize:15, color: '#476380', textAlign: 'center'}}> </Text>
					</View>
				</View>

				/*
				List of Queried information about user from github 
				*/
				<View style={{flexDirection:'row'}}>
					<View>
						<Text style = {{fontSize:15, color: '#7d9ebf'}}>{'Name: '}</Text>
					</View>
					<View>
						<Text style = {{fontSize:15, color: '#282828'}}>{this.state.name + ' '}</Text>
					</View>
				</View>
				<View style={{flexDirection:'row'}}>
					<View>
						<Text style = {{fontSize:15, color: '#7d9ebf'}}>{'Username: '}</Text>
					</View>
					<View>
						<Text style = {{fontSize:15, color: '#282828'}}>{this.state.username + ' '}</Text>
					</View>
				</View>
				<View style={{flexDirection:'row'}}>
					<View>
						<Text style = {{fontSize:15, color: '#7d9ebf'}}>{'Link: '}</Text>
					</View>
					<View>
						<Text style = {{fontSize:15, color: '#2193FF'}}>{ this.state.website + ' '}</Text>
						onPress={() => Linking.openURL(this.state.website)}
					</View>	
				</View>
				<View style={{flexDirection:'row'}}>
					<View>
						<Text style = {{fontSize:15, color: '#7d9ebf'}}>{'Email: '}</Text>
					</View>
					<View>
						<Text style = {{fontSize:15, color: '#282828'}}>{ this.state.email + ' '}</Text>
					</View>	
				</View>
				<View style={{flexDirection:'row'}}>
					<View>
						<Text style = {{fontSize:15, color: '#7d9ebf'}}>{'Created: '}</Text>
					</View>
					<View>
						<Text style = {{fontSize:15, color: '#282828'}}>{ this.state.created.substring(0,10) + ' '}</Text>
					</View>	
				</View>
				<View style={{flexDirection:'row'}}>
					<View>
						<Text style = {{fontSize:15, color: '#7d9ebf'}}>{'Last Updated: '}</Text>
					</View>
					<View>
						<Text style = {{fontSize:15, color: '#282828'}}>{ this.state.lastupdated.substring(0,10) + ' '}</Text>
					</View>	
				</View>
				
	    	</View>
	    </ScrollView>
    );
  }
}
/*
Various styles to select from
*/
const styles = StyleSheet.create({
	profilePanelStyle:{
		padding:15,
		alignItems: 'center'

	},
	buttonTextStyle:{
		//fontWeight: 'bold',

		alignItems:'center',
		fontSize:20,
		color: '#282828'
	},
	labelStyle:{
		alignItems:'center',
		fontSize:15,
		color: '#7d9ebf'
	},
	headerTitleStyle: {
      		fontWeight: 'bold',
      		fontSize:25
    	},
	headerStyle: {
       	backgroundColor: '#f4511e',
    },
  	container: {
    	marginTop:'0%',
  	},
  	profileBox:{
		flex: 1,
    	flexDirection: 'row',
    	justifyContent: 'flex-start',
    },
  	profImage:{
  		backgroundColor: 'powderblue',
		width: 100,
    	height: 100,
    	borderRadius: 50,
    	borderWidth: 1,
 		borderColor: '#3a3a3a',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
});
