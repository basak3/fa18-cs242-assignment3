import React,{ Component} from 'react'
import {Avatar} from 'react-native-elements'
import axios from 'axios'
import {Header, Icon} from 'react-native-elements'
import {Button} from 'react-native-elements';
import { 
  Text,
  View,
  Image,
  Platform,
  ScrollView,
  Animated,
  StyleSheet,
  TouchableOpacity,
  Linking,
} from 'react-native'
import { Cell, Section, TableView } from "react-native-tableview-simple";
/*
Creates a following Screen, currently set to pull 'shmarka' following data
*/
export default class FollowingScreen extends React.Component {
  static navigationOptions = {
    title: 'Following',
    headerStyle: {
          backgroundColor: '#282828',
      },  
      headerTintColor: '#fff',

  };

  /* 
   Constructor for the props selected for this page' state
   */
  constructor(props){
    super(props);
    this.state = {
      followingJSON : '',
      arrayOfCells: [],
      arrayOfURLS  : []
    }
    this.getFollowing = this.getFollowing.bind(this)
    this.createDynamicCells = this.createDynamicCells.bind(this)
  }
  /*
  This function takes a username and uses github api to query for necessary information about their followings
  param user_name : username of the user to be called in get request
  */
  getFollowing(user_name){
    axios.get('https://api.github.com/users/' + user_name + '/following')
    .then((res)=>{
      this.setState({followingJSON : res.data})

       this.createDynamicCells()
    })
  }

  /*
  Creates and populates Cells for simple-table-view of the queried followings
  */
  createDynamicCells(){
    var tempCellArray = []
    //var following_urls = []
    for(var i = 0; i < this.state.followingJSON.length; i++){
      //following_urls.push()
      //console.log(following_url)
      tempCellArray.push(
        <TouchableOpacity>
          <Cell
            cellStyle="Basic"
            title={this.state.followingJSON[i]["login"]}
            titleTextColor="#2193FF"
            
            
          ></Cell>
        </TouchableOpacity>
      )
      
    }
    this.setState({arrayOfCells : tempCellArray})
  }

  /*
  Wrapper for getfollowings, makes sure webpage is created first before populating
  */
  componentDidMount(){
    this.getFollowing('shmarka');
  }


  render() {
    return (
      <ScrollView style={styles.container}>
        /*
        Table-view of clickable followings
        */
        <Section header= "PUBLIC" >
          <TableView>
            {this.state.arrayOfCells}
          </TableView>
        </Section>
      </ScrollView>
    );
  }
}

/*
Various styles to select from
*/
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: 'light-blue',
  },
});
