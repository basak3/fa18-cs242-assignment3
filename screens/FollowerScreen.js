import React,{ Component} from 'react'
import {Avatar} from 'react-native-elements'
import axios from 'axios'
import {Header, Icon} from 'react-native-elements'
import {Button} from 'react-native-elements';
import { 
  Text,
  View,
  Image,
  Platform,
  ScrollView,
  Animated,
  StyleSheet,
  TouchableOpacity,
  Linking,
} from 'react-native'
import { Cell, Section, TableView } from "react-native-tableview-simple";
/*
Creates a follower Screen, currently set to pull 'shmarka' follower data
*/
export default class FollowerScreen extends React.Component {
  static navigationOptions = {
    title: 'Followers',
    headerStyle: {
          backgroundColor: '#282828',
      },  
      headerTintColor: '#fff',

  };

  /* 
   Constructor for the props selected for this page' state
   */
  constructor(props){
    super(props);
    this.state = {
      followerJSON : '',
      arrayOfCells: [],
      arrayOfURLS  : []
    }
    this.getFollowers = this.getFollowers.bind(this)
    this.createDynamicCells = this.createDynamicCells.bind(this)
  }
  /*
  This function takes a username and uses github api to query for necessary information about their followers
  param user_name : username of the user to be called in get request
  */
  getFollowers(user_name){
    axios.get('https://api.github.com/users/' + user_name + '/followers')
    .then((res)=>{
      this.setState({followerJSON : res.data})

       this.createDynamicCells()
    })
  }

  /*
  Creates and populates Cells for simple-table-view of the queried followers
  */
  createDynamicCells(){
    var tempCellArray = []
    //var follower_urls = []
    for(var i = 0; i < this.state.followerJSON.length; i++){
      //follower_urls.push()
      //console.log(follower_url)
      tempCellArray.push(
        <TouchableOpacity>
          <Cell
            cellStyle="Basic"
            title={this.state.followerJSON[i]["login"]}
            titleTextColor="#2193FF"
            
            
          ></Cell>
        </TouchableOpacity>
      )
      
    }
    this.setState({arrayOfCells : tempCellArray})
  }

  /*
  Wrapper for getfollowers, makes sure webpage is created first before populating
  */
  componentDidMount(){
    this.getFollowers('shmarka');
  }


  render() {
    return (
      <ScrollView style={styles.container}>
        /*
        Table-view of clickable followers
        */
        <Section header= "PUBLIC" >
          <TableView>
            {this.state.arrayOfCells}
          </TableView>
        </Section>
      </ScrollView>
    );
  }
}

/*
Various styles to select from
*/
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: 'light-blue',
  },
});
