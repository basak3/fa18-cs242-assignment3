import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import ReposScreen from '../screens/ReposScreen';
import ProfileScreen from '../screens/ProfileScreen';
import followingScreen from '../screens/FollowingScreen.js';
import followerScreen from '../screens/FollowerScreen.js';

const ProfileStack = createStackNavigator({
  Profile: ProfileScreen,
});

ProfileStack.navigationOptions = {
  tabBarLabel: 'Profile',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? `ios-options${focused ? '' : '-outline'}` : 'md-options'}
    />
  ),
};

const ReposStack = createStackNavigator({
  Repos: ReposScreen,
});

ReposStack.navigationOptions = {
  tabBarLabel: 'Repos',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? `ios-link${focused ? '' : '-outline'}` : 'md-link'}
    />
  ),
};

const FollowersStack = createStackNavigator({
  Followers: followerScreen,
});

FollowersStack.navigationOptions = {
  tabBarLabel: 'Followers',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? `ios-link${focused ? '' : '-outline'}` : 'md-link'}
    />
  ),
};

const FollowingStack = createStackNavigator({
  Following: followingScreen,
});

FollowingStack.navigationOptions = {
  tabBarLabel: 'Following',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? `ios-link${focused ? '' : '-outline'}` : 'md-link'}
    />
  ),
};


export default createBottomTabNavigator({
  ProfileStack,
  ReposStack,
  FollowersStack,
  FollowingStack,
  
  
  
  
  
});
